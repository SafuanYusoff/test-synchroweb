
<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


	$spreadsheet = new Spreadsheet();

	$inputFileType = 'Xlsx';
	$inputFileName = 'test-data.xlsx';

	/**  Create a new Reader of the type defined in $inputFileType  **/
	$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
	/**  Advise the Reader that we only want to load cell data  **/
	$reader->setReadDataOnly(true);

	/**  Get total sheet   **/
	$worksheetData = $reader->listWorksheetInfo($inputFileName);

	##LOOP ALL SHEET
	foreach ($worksheetData as $worksheet) {

		##GET SHEET NAME
		$sheetName = $worksheet['worksheetName'];

		/**  Load $inputFileName to a Spreadsheet Object  **/
		$reader->setLoadSheetsOnly($sheetName);
		$spreadsheet = $reader->load($inputFileName);

		$worksheet = $spreadsheet->getActiveSheet();


		##SHEET 1
		if($sheetName == 'Passengger By Class'){
				foreach ($worksheet->toArray() as $key => $value) {

					if($key != 0){
						$x_axis[] = $value[0];

						$row_data[0][] = $value[1];
						$row_data[1][] = $value[2];
						$row_data[2][] = $value[3];

					}
				}

				$data_sheet_1['sheet_1'] = [$row_data, $x_axis];
				$data_sheet = json_encode($data_sheet_1['sheet_1']);
		}

		##SHEET 2
		if($sheetName == 'Visitor via Social Media'){
				$pie_data[] = ['social_media', 'user'];
				foreach ($worksheet->toArray() as $key => $value) {
					$pie_data[] = [$value[0], $value[1]];
				}
				$pie_data = json_encode($pie_data);
		}

	}


?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
</head>
<body>

  <div class="container col-6">
    <div class="row">

      <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                  <p class="category">Passenger By Class</p>
              </div>
              <div class="card-content">
                  <div id="chartActivity"></div>
              </div>
          </div>

          <hr>

          <div class="card">
              <div class="card-header">
                  <p class="category">Visitor via Social Media</p>
              </div>
              <div class="card-content">
                  <div id="piechart"></div>
              </div>
          </div>
      </div>


    </div>
  </div>



  <!--  Charts Plugin -->
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
  <script type="text/javascript">


      $(document).ready(function(){
        var data = <?php echo $data_sheet ?>;
        var pie_data = <?php echo $pie_data ?>

        drawGraphChart(data[1], data[0]);
        drawPieChart(pie_data);
      })


  	function drawGraphChart(x_axis, row_data){
  		var data = {
  	      labels: x_axis,
  	      series: row_data,
  	    };

  	    var options = {
  	        seriesBarDistance: 10,
  	        axisX: {
  	            showGrid: true
  	        },
  	        height: "250px"
  	    };

  	    var responsiveOptions = [
  	      ['screen and (max-width: 640px)', {
  	        seriesBarDistance: 5,
  	        axisX: {
  	          labelInterpolationFnc: function (value) {
  	            return value[0];
  	          }
  	        }
  	      }]
  	    ];

  	    Chartist.Bar('#chartActivity', data, options, responsiveOptions);
  	}

  	function drawPieChart(data_array){
  		 //PIE CHART
  		// Load google charts
  		google.charts.load('current', {'packages':['corechart']});
  		google.charts.setOnLoadCallback(drawChart);

  		// Draw the chart and set the chart values
  		function drawChart() {
  			var data = google.visualization.arrayToDataTable(data_array);

  			// Optional; add a title and set the width and height of the chart
  			var options = {'title':'Visitor via Social Media', 'width':550, 'height':400, 'sliceVisibilityThreshold' : .0};

  			// Display the chart inside the <div> element with id="piechart"
  			var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  			chart.draw(data, options);
  		}

  	}


  </script>

</body>
</html>
