<?php
require_once "connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
  <br><br>


  <div class="container col-6">


      <p>To do App
        <a href="chart.php" style="float:right">
          <button class="btn btn-info btn-sm">Go To Chart</button>
        </a>
      </p>
      <form action="process.php" method="POST">
        <?php
        if(isset($_SESSION['error'])){
          foreach ($_SESSION['error'] as $key => $value) {
            echo '<div class="alert alert-danger" role="alert">';
            echo $value;
            echo '</div>';
          }
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          foreach ($_SESSION['success'] as $key => $value) {
            echo '<div class="alert alert-success" role="alert">';
            echo $value;
            echo '</div>';
          }
          unset($_SESSION['success']);
        }
        ?>
        <div class="input-group mb-3">

            <input type="text" name="todo" class="form-control" placeholder="What to do what to do">
            <div class="input-group-append">
              <button class="btn btn-success" type="submit">Go</button>
          </div>
        </div>
      </form>


  <table class="table table-hover">
  <thead>
    <tr>
      <th class="col-1">#</th>
      <th class="col-10 text-center">Task</th>
      <th class="col-1"></th>
    </tr>
  </thead>
  <tbody>
    <?php
      ## CONNECTION with DB -> Table task
      $sql = "SELECT * FROM task";
      $results = mysqli_query($link, $sql);
      $rows = mysqli_num_rows($results); ## CHECK NO OF ROW

      ##IF ada row in DB
      if($rows > 0){
        $i = 1;
        foreach ($results as $key => $result) {
          echo '<tr>';
          echo '<td>'. $i++ .'</td>';
          echo '<td class="text-center">'. $result['task'] .'</td>';
          echo '<td>';
          echo '<a href="process.php?action=delete&id='. $result['id'] .'">';
          echo '<button class="btn btn-danger btn-sm" onclick="return confirm(\'Are you sure to delete this record?\')">Delete</button>';
          echo '</a></td>';
          echo '</tr>';
        }
      }else{
        echo '<tr>';
        echo '<td colspan="3" class="text-center">No Results</td>';
        echo '</tr>';
      }
    ?>



  </tbody>
</table>

  </div>




</body>
</html>
