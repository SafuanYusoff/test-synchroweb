<?php

require_once "connection.php";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
  $todo = $todo_err = "";
  if(isset($_POST['todo'])){
    // Validate name
    $todo = trim($_POST["todo"]);
    if(empty($todo))
        $todo_err = "Please enter your task.";

    if(empty($todo_err)){
      $sql = "INSERT INTO task (task) VALUES (?)";
      // Bind variables to the prepared statement as parameters
      if($stmt = mysqli_prepare($link, $sql)){

        mysqli_stmt_bind_param($stmt, "s", $param_todo);

          $param_todo = $todo;
          // Attempt to execute the prepared statement
          if(mysqli_stmt_execute($stmt)){
              // Records created successfully. Redirect to landing page
              $_SESSION['success'][] = 'successfully add new record';
              header("location: index.php");
              exit();
          } else{
              echo "Something went wrong. Please try again later.";
          }
      }

    }else{
      $_SESSION['error'][] = $todo_err;
      header("location: index.php");
    }
  }
}


if($_SERVER["REQUEST_METHOD"] == "GET"){

  print_r($_GET);

  if(isset($_GET["id"]) && !empty($_GET["id"]) && $_GET['action'] == 'delete'){
    // Prepare a delete statement
    $sql = "DELETE FROM task WHERE id = ?";

    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "s", $param_id);

        // Set parameters
        $param_id = trim($_GET["id"]);

        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            // Records deleted successfully. Redirect to landing page
            $_SESSION['success'][] = 'successfully delete record';
            header("location: index.php");
            exit();
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }

  }
}

header("location: index.php");
exit();
?>
